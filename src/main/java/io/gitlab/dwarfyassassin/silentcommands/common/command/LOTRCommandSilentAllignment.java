package io.gitlab.dwarfyassassin.silentcommands.common.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import io.gitlab.dwarfyassassin.silentcommands.common.SilentSummon;
import lotr.common.LOTRLevelData;
import lotr.common.command.LOTRCommandAlignment;
import lotr.common.fac.LOTRFaction;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayerMP;

public class LOTRCommandSilentAllignment extends LOTRCommandAlignment {
    @Override
    public String getCommandName() {
        return SilentSummon.PREFIX + "alignment";
    }
    
    @Override
    public void processCommand(ICommandSender sender, String[] args) {
        if (args.length >= 2) {
            List<LOTRFaction> factions = new ArrayList<LOTRFaction>();
            if (args[1].equalsIgnoreCase("all")) {
                factions = LOTRFaction.getPlayableAlignmentFactions();
            } else {
                LOTRFaction faction = LOTRFaction.forName(args[1]);
                if (faction == null) {
                    throw new WrongUsageException("commands.lotr.alignment.noFaction", args[1]);
                }
                factions.add(faction);
            }
            if (args[0].equals("set")) {
                EntityPlayerMP entityplayer;
                float alignment = (float)LOTRCommandAlignment.parseDoubleBounded(sender, args[2], -10000.0, 10000.0);
                if (args.length >= 4) {
                    entityplayer = LOTRCommandAlignment.getPlayer(sender, args[3]);
                } else {
                    entityplayer = LOTRCommandAlignment.getCommandSenderAsPlayer(sender);
                    if (entityplayer == null) {
                        throw new PlayerNotFoundException();
                    }
                }
                for (LOTRFaction lOTRFaction : factions) {
                    LOTRLevelData.getData(entityplayer).setAlignmentFromCommand(lOTRFaction, alignment);
                    // No success messages
                    // LOTRCommandAlignment.func_152373_a(sender, this, "commands.lotr.alignment.set", entityplayer.getCommandSenderName(), lOTRFaction.factionName(), Float.valueOf(alignment));
                }
                return;
            }
            if (args[0].equals("add")) {
                float newAlignment;
                EntityPlayerMP entityplayer;
                float alignment = (float)LOTRCommandAlignment.parseDouble(sender, args[2]);
                if (args.length >= 4) {
                    entityplayer = LOTRCommandAlignment.getPlayer(sender, args[3]);
                } else {
                    entityplayer = LOTRCommandAlignment.getCommandSenderAsPlayer(sender);
                    if (entityplayer == null) {
                        throw new PlayerNotFoundException();
                    }
                }
                HashMap<LOTRFaction, Float> newAlignments = new HashMap<LOTRFaction, Float>();
                for (LOTRFaction lOTRFaction : factions) {
                    newAlignment = LOTRLevelData.getData(entityplayer).getAlignment(lOTRFaction) + alignment;
                    if (newAlignment < -10000.0f) {
                        throw new WrongUsageException("commands.lotr.alignment.tooLow", Float.valueOf(-10000.0f));
                    }
                    if (newAlignment > 10000.0f) {
                        throw new WrongUsageException("commands.lotr.alignment.tooHigh", Float.valueOf(10000.0f));
                    }
                    newAlignments.put(lOTRFaction, Float.valueOf(newAlignment));
                }
                for (LOTRFaction lOTRFaction : factions) {
                    newAlignment = newAlignments.get(lOTRFaction).floatValue();
                    LOTRLevelData.getData(entityplayer).addAlignmentFromCommand(lOTRFaction, alignment);
                    // No success messages
                    // LOTRCommandAlignment.func_152373_a(sender, this, "commands.lotr.alignment.add", Float.valueOf(alignment), entityplayer.getCommandSenderName(), lOTRFaction.factionName());
                }
                return;
            }
        }
        throw new WrongUsageException(this.getCommandUsage(sender), new Object[0]);
    }
}
