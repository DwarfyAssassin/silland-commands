package io.gitlab.dwarfyassassin.silentcommands.common.command;

import io.gitlab.dwarfyassassin.silentcommands.common.SilentSummon;
import net.minecraft.command.CommandGive;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;

public class CommandSilentGive extends CommandGive {
    @Override
    public String getCommandName() {
        return SilentSummon.PREFIX + "give";
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) {
        if(args.length < 2) {
            throw new WrongUsageException("commands.give.usage", new Object[0]);
        }
        else {
            EntityPlayerMP entityplayermp = getPlayer(sender, args[0]);
            Item item = getItemByText(sender, args[1]);
            int i = 1;
            int j = 0;

            if(args.length >= 3) {
                i = parseIntBounded(sender, args[2], 1, 64);
            }

            if(args.length >= 4) {
                j = parseInt(sender, args[3]);
            }

            ItemStack itemstack = new ItemStack(item, i, j);

            if(args.length >= 5) {
                String s = func_147178_a(sender, args, 4).getUnformattedText();

                try {
                    NBTBase nbtbase = JsonToNBT.func_150315_a(s);

                    if(!(nbtbase instanceof NBTTagCompound)) {
                        func_152373_a(sender, this, "commands.give.tagError", new Object[] {"Not a valid tag"});
                        return;
                    }

                    itemstack.setTagCompound((NBTTagCompound) nbtbase);
                }
                catch(NBTException nbtexception) {
                    func_152373_a(sender, this, "commands.give.tagError", new Object[] {nbtexception.getMessage()});
                    return;
                }
            }

            EntityItem entityitem = entityplayermp.dropPlayerItemWithRandomChoice(itemstack, false);
            entityitem.delayBeforeCanPickup = 0;
            entityitem.func_145797_a(entityplayermp.getCommandSenderName());
//            func_152373_a(sender, this, "commands.give.success", new Object[] {itemstack.func_151000_E(), Integer.valueOf(i), entityplayermp.getCommandSenderName()});
        }
    }

}
