package io.gitlab.dwarfyassassin.silentcommands.common;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import io.gitlab.dwarfyassassin.silentcommands.common.command.CommandSilentEffect;
import io.gitlab.dwarfyassassin.silentcommands.common.command.CommandSilentGive;
import io.gitlab.dwarfyassassin.silentcommands.common.command.CommandSilentSetBlock;
import io.gitlab.dwarfyassassin.silentcommands.common.command.CommandSilentSummon;
import io.gitlab.dwarfyassassin.silentcommands.common.command.LOTRCommandSilentAllignment;
import io.gitlab.dwarfyassassin.silentcommands.common.command.LOTRCommandSilentSummon;

@Mod(modid = SilentSummon.MODID, name = SilentSummon.NAME, version = SilentSummon.VERSION, acceptableRemoteVersions = "*")
public class SilentSummon {
    public static final String MODID = "silentcommands";
    public static final String NAME = "Silent Commands";
    public static final String VERSION = "1.3.0";
    public static final String PREFIX = "silent";

    @EventHandler
    public void init(FMLServerStartingEvent event) {
        event.registerServerCommand(new CommandSilentSummon());
        event.registerServerCommand(new CommandSilentGive());
        event.registerServerCommand(new CommandSilentEffect());
        event.registerServerCommand(new LOTRCommandSilentAllignment());
        event.registerServerCommand(new LOTRCommandSilentSummon());
        event.registerServerCommand(new CommandSilentSetBlock());
    }
}
