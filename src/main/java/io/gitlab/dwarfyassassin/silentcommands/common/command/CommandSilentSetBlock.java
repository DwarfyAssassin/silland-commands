package io.gitlab.dwarfyassassin.silentcommands.common.command;

import io.gitlab.dwarfyassassin.silentcommands.common.SilentSummon;
import net.minecraft.block.Block;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.NumberInvalidException;
import net.minecraft.command.WrongUsageException;
import net.minecraft.command.server.CommandSetBlock;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class CommandSilentSetBlock extends CommandSetBlock {

    @Override
    public String getCommandName() {
        return SilentSummon.PREFIX + "setblock";
    }


    @SuppressWarnings("deprecation")
    @Override
    public void processCommand(ICommandSender sender, String[] args) {
        if (args.length >= 4) {
            int i = sender.getPlayerCoordinates().posX;
            int j = sender.getPlayerCoordinates().posY;
            int k = sender.getPlayerCoordinates().posZ;
            i = MathHelper.floor_double(func_110666_a(sender, i, args[0]));
            j = MathHelper.floor_double(func_110666_a(sender, j, args[1]));
            k = MathHelper.floor_double(func_110666_a(sender, k, args[2]));
            Block block = getBlockByText(sender, args[3]);
            int l = 0;

            if (args.length >= 5) {
                l = parseIntBounded(sender, args[4], 0, 15);
            }

            World world = sender.getEntityWorld();

            if (!world.blockExists(i, j, k)) {
                throw new CommandException("commands.setblock.outOfWorld", new Object[0]);
            }
            else {
                NBTTagCompound nbttagcompound = new NBTTagCompound();
                boolean flag = false;

                if (args.length >= 7 && block.hasTileEntity()) {
                    String s = func_147178_a(sender, args, 6).getUnformattedText();

                    try {
                        NBTBase nbtbase = JsonToNBT.func_150315_a(s);

                        if (!(nbtbase instanceof NBTTagCompound)) {
                            throw new CommandException("commands.setblock.tagError", new Object[] {"Not a valid tag"});
                        }

                        nbttagcompound = (NBTTagCompound)nbtbase;
                        flag = true;
                    }
                    catch (NBTException nbtexception) {
                        throw new CommandException("commands.setblock.tagError", new Object[] {nbtexception.getMessage()});
                    }
                }

                if (args.length >= 6) {
                    if (args[5].equals("destroy")) {
                        world.func_147480_a(i, j, k, true);
                    }
                    else if (args[5].equals("keep") && !world.isAirBlock(i, j, k)) {
//                        throw new CommandException("commands.setblock.noChange", new Object[0]);
                    }
                }

                if (!world.setBlock(i, j, k, block, l, 3)) {
//                    throw new CommandException("commands.setblock.noChange", new Object[0]);
                }
                else {
                    if (flag) {
                        TileEntity tileentity = world.getTileEntity(i, j, k);

                        if (tileentity != null) {
                            nbttagcompound.setInteger("x", i);
                            nbttagcompound.setInteger("y", j);
                            nbttagcompound.setInteger("z", k);
                            tileentity.readFromNBT(nbttagcompound);
                        }
                    }

//                    func_152373_a(sender, this, "commands.setblock.success", new Object[0]);
                }
            }
        }
        else {
            throw new WrongUsageException("commands.setblock.usage", new Object[0]);
        }
    }
    
    /**
     * Gets the Block specified by the given text string.  First checks the block registry, then tries by parsing the
     * string as an integer ID (deprecated).  Warns the sender if we matched by parsing the ID.  Throws if the block
     * wasn't found.  Returns the block if it was found.
     */
    public static Block getBlockByText(ICommandSender sender, String block_ID) {
        if (Block.blockRegistry.containsKey(block_ID)) {
            return (Block)Block.blockRegistry.getObject(block_ID);
        }
        else {
            try {
                int i = Integer.parseInt(block_ID);

                if (Block.blockRegistry.containsId(i)) {
                    Block block = Block.getBlockById(i);
//                    ChatComponentTranslation chatcomponenttranslation = new ChatComponentTranslation("commands.generic.deprecatedId", new Object[] {Block.blockRegistry.getNameForObject(block)});
//                    chatcomponenttranslation.getChatStyle().setColor(EnumChatFormatting.GRAY);
//                    sender.addChatMessage(chatcomponenttranslation);
                    return block;
                }
            }
            catch (NumberFormatException numberformatexception) {}

            throw new NumberInvalidException("commands.give.notFound", new Object[] {block_ID});
        }
    }

  
}
