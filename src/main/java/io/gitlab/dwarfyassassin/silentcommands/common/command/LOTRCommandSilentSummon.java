package io.gitlab.dwarfyassassin.silentcommands.common.command;

import io.gitlab.dwarfyassassin.silentcommands.common.SilentSummon;
import lotr.common.entity.LOTREntities;

public class LOTRCommandSilentSummon extends CommandSilentSummon {
    @Override
    public String getCommandName() {
        return SilentSummon.PREFIX + "lotr_summon";
    }

    @Override
    protected String[] func_147182_d() {
        return LOTREntities.getAllEntityNames().toArray(new String[0]);
    }
}
